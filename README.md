**Notes App**
=============

Aplikácia Notes je jednoduchá fullstack webová aplikácia, ktorá demonštruje komunikáciu frontendu a backendu cez REST rozhranie na príklade CRUD operácii s poznámkami.

**Využité technológie**
-----------------------

**Frontend** :

* React nad TypeScriptom
* React UI knižnica `MUI` implementujúca material design
* Na preklady je použitá knižnica `react-i18next`

**Backend**:

* NodeJS JSON server umožňujúci CRUD operácie nad údajmi v JSON štruktúre `json-server`
* Ako databáza sa používa json súbor notes_db.json. Údajový model poznámok je jednoduchý:

                {
                        "notes": [
                                {
                                        "note": {Text poznámky},
                                        "status": {Hodnota 1 pre vytvorenú poznámku, hodnota 2 - pre splnenú poznámku},
                                        "id": {identifikátor poznámky}
                                }
                        ]
                },

**Spustenie aplikácie**
-----------------------

Aplikácia bola otestovaná na localhoste. Potrebné je spustiť zvlášť backend REST API server a frontend React development server.

Spustenie backend REST API servera
----------------------------------

1. V termináli `cd` do `/notes_be`
2. Zavolanie príkazu `npm install -g json-server`
3. Spustenie inštancie servera pomocou príkazu 
    
        json-server --watch notes_db.json --port 3001 --routes routes.json

*Poznámka: Uvedený príkaz spustí server, ktorý počúva na porte 3001. Port si upravte podľa potreby. Prepínač --routes načítava súbor routes.json, ktorým sa konfiguruje pred každý endpoint predpona /api/.*

Spustenie frontend React development servera
--------------------------------------------

1. V termináli `cd` do `/notes_fe`
2. Zavolanie príkazu `npm install`
3. Konfigurácia súboru `.env`. V rámci neho je potrebné zadať backend API URL s portom, ktorá sa používa pri volaní REST API, napr.:
        
        REACT_APP_API_URL=http://localhost:3001

4. Spustenie React development servera pomocou príkazu `npm start`
5. Spustenie aplikácie vo webovom prehliadači pomocou URL `http://localhost:3000`

**Použitie aplikácie**
------------------------

Používateľ si môže nastaviť jazyk aplikácie. Na výber sú jazyky: 

* slovenčina,
* angličtina.

**Podstránka `/` (homepage) - aplikácia *Notes***

Používateľ môže na tejto podstránke spravovať svoje poznámky. Dokáže pridávať nové poznámky, ktoré sa uložia do databázy a načítajú sa do zoznamu. V zozname následne môže upraviť stav poznámky na "splnená" a následne môže poznámku vymazať.

Implementovaná je aj validácia, kedy nie je možné vložiť prázdnu poznámku (s nulovou dĺžkou) a taktiež nie je možné vkladať poznámku dlhšiu ako 256 znakov.

**Podstránka `/about` - O aplikácii**
 
 Na tejto podstránke je statický text a tlačidlo, ktorým sa používateľ môže prekliknúť na podstránku `/` - aplikáciu *Notes*.