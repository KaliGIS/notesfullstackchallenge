import React, { useState, useEffect, useRef } from 'react';
import { Container, Grid, TextField, Typography, Button } from '@mui/material';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import NoteItem from './NoteItem';

const StyledGridItem = styled(Grid)(({theme}) => ({
    textAlign: 'center'
}));

const StyledContainer = styled(Container)({
    marginTop: '1em'
});

enum NoteStatus {
    Todo = 1,
    Done,
}

interface Note {
    id: number;
    note: string;
    status: NoteStatus;
}

function Notes() {
    const { t } = useTranslation();
    const [notes, setNotes] = useState<Note[]>([]);
    const [note, setNote] = useState<string>('');
    const [error, setError] = useState<string>('');

    useEffect(() => {
        getNotes();
    }, []);

    const handleNoteChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        console.log('In handleNoteChange');
        setNote(e.target.value);
        console.log('In handleNoteChange after setNote: ' + e.target.value);
        validateNote(e.target.value);
    }

    const getNotes = () => {
        console.log(`${process.env.REACT_APP_API_URL}/api/notes`);
        fetch(`${process.env.REACT_APP_API_URL}/api/notes`)
        .then(notesRes => {
            console.log(notesRes);
            if (notesRes.status === 200) {
                return notesRes.json();
            }
        }).then(notesData => {
            console.log(notesData);
            setNotes(notesData);
        });
    }

    const submitNote = () => {        
        const isNoteValid = validateNote(note);
        if (!isNoteValid) {
            return;
        }
        
        const newNote = {
            note,
            status: NoteStatus.Todo
        }

        setNote('');

        fetch(`${process.env.REACT_APP_API_URL}/api/notes`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newNote)
        }).then(noteRes => {
            if (noteRes.status === 201) {
                return noteRes.json();
            }
        }).then(newNote => {
            setNotes([...notes, newNote]);
        });
    }

    const validateNote = (currentNote: string): boolean => {
        console.log(currentNote.length);
        if (currentNote.length === 0) {
            setError(t('notes.error.zeroLength'));
            return false;
        } else if (currentNote.length > 256) {
            setError(t('notes.error.exceededLength'));
            return false;
        }

        setError('');
        return true;
    }

    const updateNoteCb = (updatedNote: Note) => {
        const updatedNotes = notes.map(note => {
            if (note.id === updatedNote.id) {
                return updatedNote;
            }
            return note;
        });
        
        setNotes(updatedNotes);
    }

    const deleteNoteCb = (deletedNote: Note) => {
        const updatedNotes = notes.filter(note => {
            return note.id !== deletedNote.id;
        });
        
        setNotes(updatedNotes);
    }

    const handleKeyDown = (e: React.KeyboardEvent) => {
        if (e.code === 'Enter') {
            submitNote();
        }
    }

    const renderNotes = () => {
        return notes.map(note => {
            return (
                <StyledGridItem item xs={12} key={note.id}>
                    <NoteItem note={note} updateNoteCb={updateNoteCb} deleteNoteCb={deleteNoteCb} />
                </StyledGridItem>
            );
        });
    }

    return (
        <StyledContainer maxWidth="md">
            <Grid container spacing={4}>
                <StyledGridItem item xs={12}>
                    <Typography variant='h5' component='h5'>{t('notes.heading')}</Typography>
                </StyledGridItem>
                <StyledGridItem item xs={12}>
                    {
                        !error ?
                            <TextField
                                name="note"
                                label={t('notes.note')}
                                variant="outlined"
                                type="text"
                                fullWidth={true}
                                value={note}
                                onChange={handleNoteChange}
                                onKeyDown={handleKeyDown}
                            />
                        :
                            <TextField
                                error
                                helperText={error}
                                name="note"
                                label={t('notes.note')}
                                variant="outlined"
                                type="text"
                                fullWidth={true}
                                value={note}
                                onChange={handleNoteChange}
                                onKeyDown={handleKeyDown}
                            />
                    }
                </StyledGridItem>
                <StyledGridItem item xs={12}>
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        onClick={submitNote}
                        disabled={error ? true : false}
                    >
                        {t('notes.submit')}
                    </Button>
                </StyledGridItem>
                {
                    notes && renderNotes()
                }
            </Grid>
        </StyledContainer>
    )
}

export default Notes;
