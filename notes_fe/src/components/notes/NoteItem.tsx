import React from 'react';
import { Grid, IconButton, Typography } from '@mui/material';
import CheckIcon from '@mui/icons-material/Check';
import DeleteIcon from '@mui/icons-material/Delete';

enum NoteStatus {
    Todo = 1,
    Done,
}

interface Note {
    id: number;
    note: string;
    status: NoteStatus;
}

interface NoteItemProps {
    note: Note;
    updateNoteCb: (note: Note) => void;
    deleteNoteCb: (note: Note) => void;
}

function NoteItem({note, updateNoteCb, deleteNoteCb}: NoteItemProps) {
    const changeStatusToDone = () => {
        const noteToUpdate = {
            ...note,
            status: NoteStatus.Done
        };

        fetch(`${process.env.REACT_APP_API_URL}/api/notes/${note.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(noteToUpdate)
        }).then(noteRes => {
            if (noteRes.status === 200) {
                return noteRes.json();
            }
        }).then(updatedNote => {
            return updateNoteCb(updatedNote);
        });
    }

    const deleteNote = () => {
        fetch(`${process.env.REACT_APP_API_URL}/api/notes/${note.id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            }
        }).then(res => {
            if (res.status === 200) {
                return deleteNoteCb(note);
            }
        });
    }

    const renderActionButton = () => {
        if (note.status === NoteStatus.Todo) {
            return (
                <IconButton onClick={changeStatusToDone}>
                    <CheckIcon />
                </IconButton>        
            ); 
        } else {
            return (
                <IconButton onClick={deleteNote}>
                    <DeleteIcon />
                </IconButton>        
            );
        }
    }

    return (
        <Grid container alignItems='center'>
            <Grid item xs={11} textAlign='left'>
                <Typography 
                    component={'p'} 
                    noWrap={true}
                    sx={{textDecoration: (note.status === NoteStatus.Done) ? 'line-through' : 'none'}}
                >
                    {note.note}
                </Typography>
            </Grid>
            <Grid item xs={1} textAlign='right'>
                {
                    renderActionButton()
                }
                    
            </Grid>
        </Grid>
    )
}

export default NoteItem;
