import { Container, Typography, Grid, Button } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { styled } from '@mui/system';

const StyledContainer = styled(Container)({
    marginTop: '1em'
});

function About() {
    const { t } = useTranslation();
    console.log('dotenv.API_URL: ' + process.env.REACT_APP_API_URL)

    return (
        <StyledContainer maxWidth='sm'>
            <Grid container spacing={4} textAlign='center'>
                <Grid item xs={12}>
                    <Typography component='h5' variant='h5'>
                        {t('about.heading')}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography component='p'>
                        Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa egestas mollis varius;
                        dignissim elementum. Mollis tincidunt mattis hendrerit dolor eros enim, nisi ligula ornare.
                        Hendrerit parturient habitant pharetra rutrum gravida porttitor eros feugiat. Mollis elit
                        sodales taciti duis praesent id. Consequat urna vitae morbi nunc congue.
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Button variant='contained' to='/' component={Link}>
                        {t('about.goToNotes')}
                    </Button>
                </Grid>
            </Grid>
        </StyledContainer>
    );
}

export default About;
