import React from 'react';
import { AppBar, Typography } from '@mui/material';
import { styled } from '@mui/system';

const BottomAppBar = styled(AppBar)(({theme}) => ({
    top: 'auto',
    bottom: 0
}));

function Footer() {
    return (
        <BottomAppBar position='sticky' color='secondary'>
            <Typography component='p' textAlign='center' color={'primary.dark'}>
                Martin Kalivoda © 2021
            </Typography>
        </BottomAppBar>
    )
}

export default Footer;
