import React from 'react';
import { AppBar, Button, Stack } from '@mui/material'
import logo from './notesLogo.png';
import { Link, useLocation } from 'react-router-dom';
import LanguageMenu from './LanguageMenu';
import { useTranslation } from 'react-i18next';
import { styled } from '@mui/system';

const StyledButton = styled(Button)(({theme}) => ({
    '&.active': {
        'background-color': theme.palette.grey['200'],
    }
}));

const StyledLogo = styled('img')({
    margin: '8px'
});

function Header() {
    const { t } = useTranslation();
    const location = useLocation();
    const splitLocation = location.pathname.split('/');

    return (
        <AppBar position='sticky' color='secondary'>
            <Stack direction='row' alignItems='center' justifyContent='space-between'>
                <StyledLogo src={logo} width='50' height='60' />
                <Stack direction='row' spacing={1} alignItems='center'>
                    <Link to='/'>
                        <StyledButton 
                            className={splitLocation[1] === '' ? 'active' : ''}
                        >
                                {t('header.notesPage')}
                        </StyledButton >
                    </Link>
                    <Link to='/about'>
                        <StyledButton  
                            className={splitLocation[1] === 'about' ? 'active' : ''}
                        >
                                {t('header.aboutPage')}
                        </StyledButton >
                    </Link>
                    <LanguageMenu />
                </Stack>
            </Stack>
        </AppBar>
    )
}

export default Header;
