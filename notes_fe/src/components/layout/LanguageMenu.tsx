import React, { useState } from 'react';
import { Menu, MenuItem, Button } from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import i18n from '../../translations/i18n';

function LanguageMenu() {
    const [currentLanguage, setCurrentLanguage] = useState(i18n.language);
    const [anchorElement, setAnchorElement] = useState<null | HTMLElement>(null);
    const menuOpen = Boolean(anchorElement);
    
    const handleButtonClick = (e: React.MouseEvent<HTMLElement>) => {
        setAnchorElement(e.currentTarget);        
    }

    const handleMenuClose = () => {
        setAnchorElement(null);
    }

    const handleLanguageChange = (language: string) => {
        i18n.changeLanguage(language);
        setCurrentLanguage(language);
    }

    return (
        <React.Fragment>
            <Button
                variant='text'
                endIcon={<KeyboardArrowDownIcon />}
                onClick={handleButtonClick}
                fullWidth={false}
            >
                {currentLanguage}
            </Button>
            <Menu
                open={menuOpen}
                anchorEl={anchorElement}
                onClose={handleMenuClose}
            >
                <MenuItem onClick={() => handleLanguageChange('sk')}>
                    SK
                </MenuItem>
                <MenuItem value='en' onClick={() => handleLanguageChange('en')}>
                    EN
                </MenuItem>
            </Menu>
        </React.Fragment>
    )
}

export default LanguageMenu;
