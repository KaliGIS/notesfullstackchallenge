import React from 'react';
import Notes from './components/notes/Notes';
import About from './components/about/About';
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';
import { Route, Routes } from 'react-router-dom';
import './translations/i18n';
import { styled, ThemeProvider } from '@mui/system';
import notesAppTheme from './utils/theme';

const Wrapper = styled('div')({
  height: '100vh',
  display: 'flex',
  flexDirection: 'column',
});

const StyledRoutes = styled(Routes)(({theme}) => ({
  'flex': 1,
  'overflow': 'auto'
}));

const Content = styled('div')({
  'flex': 1,
  'overflow': 'auto'
});

function App() {
  return (
    <ThemeProvider theme={notesAppTheme} >      
      <Wrapper>
        <Header />
        <Content>
          <Routes>
            <Route path="/" element={<Notes />} />
            <Route path="/about" element={<About/>} />
          </Routes>
        </Content>
        <Footer />
      </Wrapper>
    </ThemeProvider>
  );
}

export default App;
