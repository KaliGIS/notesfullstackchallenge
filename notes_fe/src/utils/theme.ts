import { createTheme } from '@mui/material/styles';

declare module '@mui/material/styles' {
    interface Theme {
        status: {
            danger: string;
        };
    }
    
    // allow configuration using `createTheme`
    interface ThemeOptions {
        selectedButton?: {
            background?: string;
        };
    }
}

const notesAppTheme = createTheme({
    selectedButton: {
        background: 'yellow'
    },
    palette: {
        secondary: {
            main: 'rgb(209,176,202)',
            dark: 'rgb(143,123,139)'
        }
    }
});

export default notesAppTheme;