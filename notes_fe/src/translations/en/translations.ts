export const TRANSLATIONS_EN = {
    header: {
        notesPage: 'Notes',
        aboutPage: 'About'
    },
    notes: {
        heading: 'Notes',
        submit: 'Add',
        note: 'add note ...',
        error: {
            zeroLength: 'You cannot add an empty note',
            exceededLength: 'Maximum length of note is 256 characters'
        }
    },
    about: {
        heading: 'About',
        goToNotes: 'Go to notes'
    }
};