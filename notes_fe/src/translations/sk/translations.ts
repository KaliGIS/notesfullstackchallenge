export const TRANSLATIONS_SK = {
    header: {
        notesPage: 'Poznámky',
        aboutPage: 'O aplikácii'
    },
    notes: {
        heading: 'Poznámky',
        submit: 'Odošli',
        note: 'pridaj poznámku ...',
        error: {
            zeroLength: 'Nemôžete pridať prázdnu poznámku',
            exceededLength: 'Maximálna dĺžka poznámky môže mať 256 znakov'
        }
    },
    about: {
        heading: 'O aplikácii',
        goToNotes: 'Choď na poznámky'
    }
};